﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class Player : Entity, IAttack
    {
        [SerializeField]
        private int currentHealth;

        [SerializeField] 
        private int attackAmount;

        [SerializeField] 
        private bool isAlive;
        
        public int CurrentHealth
        {
            get { return currentHealth; }
            set { currentHealth = value; }
        }


        public int AttackAmount
        {
            get { return attackAmount; }
            set { attackAmount = value; }
        }


        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }

        public Enemy enemyEntity;

        private void Start()
        {
            Log(EntityName);
            Log(currentHealth);
            Log(attackAmount);
            AttackEnemy(enemyEntity, attackAmount);

        }

        public void AttackEnemy(Enemy enemy,  int AttackDamage)
        {
            if (enemy == isAlive)
            {
                AttackEnemy(enemy, 100);
            }
        }
    }

    
}