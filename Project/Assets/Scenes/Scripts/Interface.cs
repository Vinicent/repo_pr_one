﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{

    public interface IAttack
    {
        void AttackEnemy(Enemy enemy, int amountAttackDamage);
    }

        public interface IDamagable
        {
            void Damage(int amountDamage);
        }


        public interface ILog
        {
            void log(object needsToBeLogged);
        }
     
}

