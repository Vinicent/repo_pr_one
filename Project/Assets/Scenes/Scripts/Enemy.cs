﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class Enemy : Entity, IDamagable
    {
        [SerializeField]
        private int currentHealth;

        [SerializeField] 
        private int attackAmount;

        [SerializeField] 
        private bool isAlive;


        public bool IsAlive
        {
            get {return isAlive;} 
            set {isAlive = value;} 
        }

        public void Damage(int amountDamage)
        {
            if (currentHealth > 0)
            {
                currentHealth -= amountDamage;

                if (currentHealth <= 0)
                {
                    currentHealth = 0;
                    isAlive = false;
                    Debug.Log("Feind: " + currentHealth);
                    return;
                }
            }
            else if (currentHealth <= 0)
            {
                currentHealth = 0;
                isAlive = false;
                Debug.Log("Feind: " + currentHealth);
            }
        }
    }
}


